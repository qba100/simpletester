/***************************************************************************
 *   Copyright (C) 2012 by lemm                                            *
 *   ten.michal.k@gmail.com                                                *
 *                                                                         *
 *   All rights reserved.                                                  *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS   *
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT     *
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR *
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR *
 *   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, *
 *   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,   *
 *   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR    *
 *   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF *
 *   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING  *
 *   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS    *
 *   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.          *
 ***************************************************************************/


#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <vector>
#include <list>
#include <ctime>
#include <algorithm>
#include <locale>


using namespace std;

class CQuestion{
	private:
		wchar_t poprawna;
		wstring pytanie;
		vector<wstring> odpowiedzi;
		int liczba_wyswietlen;
	public:
		CQuestion(wstring pyt) : pytanie(pyt), liczba_wyswietlen(0) {}
		void DodajOdp(wstring& odp){
			odpowiedzi.push_back(odp);
		}
		void Wyswietlono(){ liczba_wyswietlen++; }
		int LiczbaWyswietlen() const {return liczba_wyswietlen; } 
		wstring& Pytanie() { return pytanie; }
		wchar_t Poprawna(){return poprawna;}
		void Poprawna(wchar_t _poprawna){poprawna=_poprawna;}
		vector<wstring>& Odpowiedzi(){ return odpowiedzi; }
};

int pyt_sort(const CQuestion &question1, const CQuestion &question2){
	return question1.LiczbaWyswietlen() < question2.LiczbaWyswietlen();
}

int bye(int status = 0){
	if(status!=0) wcout << L"BŁAD WCZYTYWANIA PLIKU" << endl;
	wcout << L"Thx, bye!" << endl;

	string stop;
	getline(cin, stop);

	return status;
}

void wypisz_baze(vector<CQuestion> &pytania){
	int size = pytania.size();
	for(int i=0; i<size;i++){
		wcout << i << L" " <<pytania[i].Pytanie() << endl;
		int lpyt = pytania[i].Odpowiedzi().size();
		for(int j=0; j<lpyt; j++) 
			wcout << pytania[i].Odpowiedzi()[j] << endl;
		wcout << (char)(pytania[i].Poprawna()+'0') << endl;
	}
}
int main(int argc, char* argv[])
{

	bool dziala = true;
	vector<CQuestion> pytania;
	bool blad = true;
	srand(time(NULL));

	setlocale( LC_ALL, "C.UTF-8" );
	wcout << "Tester ver 1.1" << endl;
	wcout << "Lemm @ 2012 / Qba100 (Add utf-8 support) @ 2019" << endl;

	//wczytaj dane
	wifstream plik("questions.txt", wifstream::in);
	std::locale loc("");
	plik.imbue(loc);

	wstring temp;
	int lpytan;
	wchar_t odp;
	wstring ver;
	if(!plik.good())
		return bye(1);
	else
		getline(plik, ver);

	while(plik.good()){
		blad = false;
		getline(plik, temp);
		//nowe pytanie
		if(!temp.empty()){
			CQuestion nowe(temp);
			plik >> lpytan;

			//wyciecie entera
			getline(plik, temp);
			int i=0;
			for(i=0; i<lpytan && plik.good(); i++) {
				getline(plik, temp);
				nowe.DodajOdp(temp);
			}
			if(i==lpytan){
				plik >> odp;
				//wyciecie entera
				getline(plik, temp);
				//mo¿e te¿ zamiast odp dobrej 1,2,3 byæ a,b,c lub A,B,C
				if(!isdigit(odp)){
					odp = tolower(odp) - 'a' + 1;//bo numerujemy od 1ki odpowiedzi
				} else  {
					odp = odp - '0';
				}

				if(odp >=0 && odp <= 10){ //max mozliwych odp to 10, reszta to blad...
					//wyglada na to ze z pytaniem wszystko ok..
					nowe.Poprawna(odp);
					pytania.push_back(nowe);
				}else {
					blad = true;
				}
			}
		}
	}

	if (blad) {
		wstring ui;
		wcout << L"Bazę danych wczytano z błedami" << endl;
		wcout << L"Pytań w bazie: " << pytania.size() << L". Czy chcesz kontynuować?(T/N)" << endl;
		char fst;
		do{
			wcin >> ui;
			fst = ui[0];
		} while(fst != 'y' &&
			fst!= 'Y' &&
			fst!= 'N' &&
			fst!= 'n' &&
			fst!= 'T' &&
			fst!= 't');
		if(fst == 'n' || fst == 'N') 
			return bye(0);
	} else {
		wcout << L"Bazę danych wczytano poprawnie" << endl;
		wcout << L"Pytań w bazie: " << pytania.size() << endl;
	}

	//wypisz_baze(pytania);
	wcout << "Jezeli chcesz zakonczyc aktualny test wprowadz \"z\" w dowolnym momencie. \nJezeli chcesz zobaczyc aktualny wynik wprowadz W \nOdpowiadaj na pytania A,B lub C \nNacisnij enter jezeli jestes gotow na 1sze pytanie"<< endl;
	wstring ans;
	getline(wcin,ans);
	int nrpyt = 0;
	int poprawne = 0;
	while(dziala){
		//niech wybiera pytania najmniej razy pokazane, lub ze zlymi odp.
		sort(pytania.begin(),pytania.end(), pyt_sort);
		int los = rand()%(pytania.size())/4;

		wcout << pytania[los].Pytanie() << endl;
		int lpyt = pytania[los].Odpowiedzi().size();
		for(int i=0; i<lpyt; i++) 
			wcout << (char)(i+'A')<<") "<< pytania[los].Odpowiedzi()[i] << endl;
		char fst = 0;
		do{
			getline(wcin,ans);
			fst = tolower(ans[0]);
			if(fst=='z') {
				wcout << "Twoj wynik to: " << (double)((double)poprawne/(double)(nrpyt==0? 1 : nrpyt))*100.0f << "%" << endl;
				return bye(0);}
			else if(fst=='w') {
				wcout << "Twoj aktualny wynik to: " << (double)((double)poprawne/(double)(nrpyt==0? 1 : nrpyt))*100.0f << "%" << endl;
				//zeby dalej mozna bylo wprowadzac odp..
				fst = 0;
			}

		}while(!(fst>='a' && fst<='z'));
		//spr odpowiedz

		int odp = tolower(fst)-'a';
		if(odp+1 == pytania[los].Poprawna()) {
			wcout << L"Brawo! To byla poprawna odpowiedź" << endl;
			poprawne++;
			pytania[los].Wyswietlono();
		} else {
			wcout << L"To niestety zla odpowiedz. Poprawna to: "<< (wchar_t)(pytania[los].Poprawna() + 'A' - 1) << endl;
		}
		nrpyt++;
		wcout << endl;
	}
	dziala = false;


	bye(0);

	return 0;
}

